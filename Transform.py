import os
import json
import pandas as pd
import csv
import us
import gstate
entries = os.listdir('Dumps')
for x in entries:
    #print(x)
    y=x.split(".")
    read_file = pd.read_excel (r"Dumps/"+x)
    read_file.to_csv ("CSV/csv_"+y[0]+".csv", index = None, header=True)

with open('country.json') as f:
        data = json.load(f)

    

fentries = os.listdir('CSV')
for x in fentries:
    with open('CSV/'+x,'r',encoding="utf8") as csvinput:
        with open("Transformed/Trans_"+x, 'w',encoding="utf8") as csvoutput:
            writer = csv.writer(csvoutput, lineterminator='\n')
            reader = csv.reader(csvinput)
            y=0    
            all = []
            OPC=0
            OPS=0
            j=0 
            f=0                            #all2 = []
            try:                               
               row = next(reader)
            except Exception as ex:
                 print("Transforming...")
            all.append(row)
            for r1 in all:
                                                #print(r)                            #print(r[6])
                for x1 in r1:
                                                        #p=x1                           
                    if x1.strip() == 'Opportunity Country (This is the shipping country)' or x1.strip() == 'Opportunity Country':
                                            #print(x+" index: "+str(i))
                        OPC=j
                    if x1.strip() == 'State/Province of Opportunity(This the shipping state)' or x1.strip() == 'State/Province of Opportunity' or x1.strip() =='State/Province of Opportunity(This the shipping state) ONLY APPLIED FOR USA AND CANADA':

                            #rint("here"+x1)                          #print(x+" index: "+str(i))
                        OPS=j
                    
                    j=j+1
            for row in reader:
                #print("Oppty country "+row[OPC])
                #print("Oppty state "+row[OPC])
                f=0
                for cname in data["Country"]:
                    #print(row[OPC].strip()+cname.strip())
                    if row[OPC].strip()==cname.strip():
                        f=1

                #print("true"+ str(f) +"for "+row[OPC])
                if f==0:
                    #print(row[OPC])
                    if row[OPC].strip()=='Korea, Republic of':
                        row[OPC]='Korea, South'
                        #print("here")
                    if  row[OPC].strip()=="Korea, Democratic People's":
                         row[OPC]='Korea, North'
                    if  row[OPC].upper().strip().__contains__("USA"):
                        #print()
                        row[OPC]='United States'
                    if  row[OPC].upper().strip().__contains__("UAE"):
                         row[OPC]='United Arab Emirates'
                    elif row[OPC].lower().__contains__('india'):
                         row[OPC]="India"
                    elif row[OPC].lower().__contains__('qatar') or row[OPC].lower().__contains__('qater'):
                         row[OPC]="Qatar"
                    elif row[OPC].lower().__contains__('yemen') or row[OPC].lower().__contains__('yeman'):
                         row[OPC]="Yemen"
                    elif row[OPC].lower().__contains__('test'):
                         row[OPC]="Canada"
                         row[OPS]='Quebec'
                    else:
                        for cname in data["Country"]:
                            if row[OPC].lower().strip().__contains__(cname.lower().strip()):

                                row[OPC]=cname.strip()

                         
                if row[OPC].strip()=='USA' or row[OPC].strip()=='United States' or  row[OPC].lower().strip()=='canada' :
                    if row[OPS]:
                        try:
                            row[OPS]=gstate.getstate(row[OPS].upper())
                        except Exception as ex:
                             print("Transforming...")
                    #print(row[OPC])
                all.append(row)
            writer.writerows(all)
print("Transformed completed")

